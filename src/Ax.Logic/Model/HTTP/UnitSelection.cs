﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ax.Logic.Model.HTTP
{
    public sealed class UnitSelection
    {
        public IReadOnlyList<string> Names { get; }
        public IReadOnlyList<int> Numbers { get; }

        public UnitSelection(IEnumerable<string> names, IEnumerable<int> numbers)
        {
            Names = names?.ToList() ?? throw new ArgumentNullException(nameof(names));
            Numbers = numbers?.ToList() ?? throw new ArgumentNullException(nameof(numbers));
        }

        public string ToJson()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }
}