﻿using System;
using System.Collections.Generic;
using Ax.Logic.Model.UDP;

namespace Ax.Logic.Model.Map
{
    public static class UdpDataExtensions
    {
        public static bool IsNeighborOf(this UdpData data1, UdpData data2)
        {
            if (data1.X == data2.X && data1.Y == data2.Y)
                throw new InvalidOperationException("Overlapping objects, oops!");

            int xDiff = Math.Abs(data1.X - data2.X);
            int yDiff = Math.Abs(data1.Y - data2.Y);

            // HANDLE too far
            if (xDiff > 1 || yDiff > 1)
                return false;

            // Only near

            bool isNextToEachOtherX = xDiff == 1 && yDiff == 0;
            bool isNextToEachOtherY = yDiff == 1 && xDiff == 0;

            // HANDLE normal neighbors
            if (isNextToEachOtherX || isNextToEachOtherY)
                return true;

            // HANDLE extra neighbors
            if (data1.Y % 2 == 0 && data1.X < data2.X)
                return true;
            else if (data2.Y % 2 == 0 && data1.X > data2.X)
                return true;


            return false;
        }

        public static List<UdpData> GetNeighbors(this UdpData referenceUnit, MapEntry[,] map)
        {
            if (referenceUnit == null)
                throw new ArgumentNullException(nameof(referenceUnit));
            if (map == null)
                throw new ArgumentNullException(nameof(map));

            // Alright, this is not the fastest but I don't want to waste anymore time with things like this.
            List<UdpData> neighbors = new List<UdpData>();

            foreach (MapEntry mapEntry in map)
            {
                UdpData currentUnit = mapEntry.UdpData;

                if (currentUnit == null || currentUnit.Equals(referenceUnit))
                    continue;

                if (mapEntry.UdpData.IsNeighborOf(referenceUnit))
                    neighbors.Add(mapEntry.UdpData);
            }

            return neighbors;
        }
    }
}