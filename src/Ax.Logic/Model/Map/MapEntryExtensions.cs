﻿using Ax.Logic.Model.UDP;

namespace Ax.Logic.Model.Map
{
    public static class MapEntryExtensions
    {
        public static void DeletePreviousUdpData(this MapEntry[,] map, UdpData newData)
        {
            for (int y = 0; y < map.GetLength(0); y++)
            {
                for (int x = 0; x < map.GetLength(1); x++)
                {
                    UdpData currentData = map[y, x]?.UdpData;

                    if (currentData == null)
                        continue;

                    if (currentData.Equals(newData))
                    {
                        map[y, x] = MapEntry.CreateEmpty(y, x);
                    }
                }
            }
        }
    }
}