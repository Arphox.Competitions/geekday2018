﻿using System;

namespace Ax.Logic.MathFuck
{
    public static class MoveCalculator
    {
        public static Point CalculateNextMove(Point source, Point target, int speed)
        {
            double dist = GetDistance(source, target);
            Vector dirVector = new Vector(target.X - source.X, target.Y - source.Y);
            Vector unitVector = new Vector(dirVector.X / dist, dirVector.Y / dist);

            int resultX = 0;
            int resultY = 0;

            if (dist > speed)
            {
                resultX = Convert.ToInt32(Math.Floor(source.X + unitVector.X * speed));
                resultY = Convert.ToInt32(Math.Floor(source.Y + unitVector.Y * speed));
                return new Point(resultX, resultY);
            }

            resultX = Convert.ToInt32(Math.Floor(source.X + unitVector.X * dist));
            resultY = Convert.ToInt32(Math.Floor(source.Y + unitVector.Y * dist));
            return new Point(resultX, resultY);
        }

        private static double GetDistance(Point p1, Point p2)
        {
            double pow1 = Math.Pow(p2.X - p1.X, 2);
            double pow2 = Math.Pow(p2.Y - p1.Y, 2);
            double sqrt = Math.Sqrt(pow1 + pow2);
            return sqrt;
        }
    }
}