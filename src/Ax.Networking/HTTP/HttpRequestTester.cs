﻿using System;
using System.Diagnostics;
using System.Net.Http;

namespace Ax.Networking.HTTP
{
    public static class HttpRequestTester
    {
        public static void OneTest()
        {
            const string getUrl = "http://192.168.1.67/homm?squadMoney=57596";

            HttpClient client = new HttpClient();
            Stopwatch sw = Stopwatch.StartNew();
            var result = client.GetAsync(getUrl).ConfigureAwait(false).GetAwaiter().GetResult();
            Console.WriteLine(sw.ElapsedMilliseconds);
        }
    }
}