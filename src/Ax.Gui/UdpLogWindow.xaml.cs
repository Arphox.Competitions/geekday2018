﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Threading;

namespace Ax.Gui
{
    public partial class UdpLogWindow : Window
    {
        private Queue<byte[]> dataQueue = new Queue<byte[]>();

        public UdpLogWindow()
        {
            InitializeComponent();

            DispatcherTimer processTimer = new DispatcherTimer();
            processTimer.Interval = TimeSpan.FromMilliseconds(500);
            processTimer.Tick += ProcessTimer_Tick;
            processTimer.Start();
        }

        private void ProcessTimer_Tick(object sender, EventArgs e)
        {
            while (dataQueue.Count > 0)
            {
                byte[] data = dataQueue.Dequeue();
                ProcessEntry(data);
            }
        }

        public void Log(byte[] data)
        {
            dataQueue.Enqueue(data);
        }

        private void ProcessEntry(byte[] data)
        {
            string message = Encoding.UTF8.GetString(data);
            string datePrefix = DateTime.Now.ToString("yyyy-MM-dd HH.mm.ss") + ": ";

            Dispatcher.Invoke(() =>
            {
                listbox_log.Items.Insert(0, datePrefix + message);
            });
        }
    }
}